package facci.merchan.telefonosql;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText Imai, Gama, Modelo, Marca, Compania, id;
    private Button guardar, leer, leerT, actualizar, Eliminar, eliminarT;
    private TextView Datos;
    private DataBase dataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Imai = (EditText)findViewById(R.id.imai);
        Gama = (EditText)findViewById(R.id.Igama);
        Modelo = (EditText)findViewById(R.id.Imodelo);
        Marca = (EditText)findViewById(R.id.Imarca);
        Compania = (EditText)findViewById(R.id.Icompania);
        id = (EditText)findViewById(R.id.Id);
        guardar = (Button)findViewById(R.id.Guardar);
        leerT = (Button)findViewById(R.id.LeerT);
        actualizar = (Button)findViewById(R.id.Actualizar);
        eliminarT = (Button)findViewById(R.id.EliminarT);
        //Eliminar = (Button)findViewById(R.id.Eliminar);
        //leer = (Button)findViewById(R.id.Leer);

        guardar.setOnClickListener(this);
        leerT.setOnClickListener(this);
        //leer.setOnClickListener(this);
        //Eliminar.setOnClickListener(this);
        actualizar.setOnClickListener(this);
        eliminarT.setOnClickListener(this);
        Datos = (TextView)findViewById(R.id.Datos);
        dataBase = new DataBase(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.Guardar:

                if (Imai.getText().toString().isEmpty()){

                }else if(Imai.getText().toString().isEmpty()){

                }else if(Gama.getText().toString().isEmpty()){

                }else if (Modelo.getText().toString().isEmpty()){

                }else if(Marca.getText().toString().isEmpty()){

                }else if(Compania.getText().toString().isEmpty()){

                }else {
                    dataBase.Insertar(Imai.getText().toString(), Gama.getText().toString(),
                            Modelo.getText().toString(), Marca.getText().toString(), Compania.getText().toString());
                    Toast.makeText(this, "GUARDADO", Toast.LENGTH_SHORT).show();
                    id.setText("");
                    Imai.setText("");
                    Gama.setText("");
                    Modelo.setText("");
                    Marca.setText("");
                    Compania.setText("");
                    Datos.setText("");
                }

                break;

            case R.id.LeerT:
                Datos.setText(dataBase.LeerTodos());
                id.setText("");
                Imai.setText("");
                Gama.setText("");
                Modelo.setText("");
                Marca.setText("");
                Compania.setText("");
                Toast.makeText(this, "LISTADOS", Toast.LENGTH_SHORT).show();
                break;

            /*case R.id.Leer:
                Datos.setText(dataBase.Leer(id.getText().toString()));
                id.setText("");
                Siglas.setText("");
                Nombres.setText("");
                Carreras.setText("");
                Categoria.setText("");
                Ciudad.setText("");
                Toast.makeText(this, "LISTADO", Toast.LENGTH_SHORT).show();

                break;
            */
            /*case R.id.Eliminar:
                dataBase.Eliminar(id.getText().toString());
                id.setText("");
                Siglas.setText("");
                Nombres.setText("");
                Carreras.setText("");
                Categoria.setText("");
                Ciudad.setText("");
                Datos.setText("");
                Toast.makeText(this, "ELIMINADO", Toast.LENGTH_SHORT).show();
                break; */

            case R.id.Actualizar:
                if (Imai.getText().toString().isEmpty()){

                }else if(Imai.getText().toString().isEmpty()){

                }else if(Gama.getText().toString().isEmpty()){

                }else if (Modelo.getText().toString().isEmpty()){

                }else if(Marca.getText().toString().isEmpty()){

                }else if(Compania.getText().toString().isEmpty()){

                }else if(id.getText().toString().isEmpty()){

                }else {
                    dataBase.Actualizar(id.getText().toString(), Imai.getText().toString(), Gama.getText().toString(),
                            Modelo.getText().toString(), Marca.getText().toString(), Compania.getText().toString());
                    Toast.makeText(this, "ACTUALIZADO", Toast.LENGTH_SHORT).show();
                    id.setText("");
                    Imai.setText("");
                    Gama.setText("");
                    Modelo.setText("");
                    Marca.setText("");
                    Compania.setText("");
                    Datos.setText("");
                }
                break;

            case R.id.EliminarT:
                dataBase.EliminarTodo();
                Datos.setText("");
                Toast.makeText(this, "ELIMINADOS", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
