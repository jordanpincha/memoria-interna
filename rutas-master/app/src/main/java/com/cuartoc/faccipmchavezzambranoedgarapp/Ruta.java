package com.cuartoc.faccipmchavezzambranoedgarapp;

import com.orm.SugarRecord;

public class Ruta extends SugarRecord {

    String ruta, origen, destino, compañia, recorrido;

    public Ruta() {
    }

    public Ruta(String ruta, String origen, String destino, String compañia, String recorrido) {
        this.ruta = ruta;
        this.origen = origen;
        this.destino = destino;
        this.compañia = compañia;
        this.recorrido = recorrido;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getCompañia() {
        return compañia;
    }

    public void setCompañia(String compañia) {
        this.compañia = compañia;
    }

    public String getRecorrido() {
        return recorrido;
    }

    public void setRecorrido(String recorrido) {
        this.recorrido = recorrido;
    }
}
