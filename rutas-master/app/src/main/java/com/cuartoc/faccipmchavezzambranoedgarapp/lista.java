package com.cuartoc.faccipmchavezzambranoedgarapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class lista extends AppCompatActivity {

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        listView = (ListView) findViewById(R.id.listViewRutas);
        cargarListView();
    }

    private void cargarListView() {
        List<Ruta> resultadoConsulta = Ruta.listAll(Ruta.class);
        ArrayList<String> lista = new ArrayList<String>();
        for(int i=0; i<resultadoConsulta.size();i++){
            Ruta resultado = resultadoConsulta.get(i);
            lista.add(resultado.getRuta()+" - "+resultado.getOrigen() +" - "+resultado.getDestino()+" - "+resultado.getCompañia() +" - "+resultado.getRecorrido());
        }
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, lista);
        listView.setAdapter(adaptador);
    }
}
