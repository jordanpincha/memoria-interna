package com.cuartoc.faccipmchavezzambranoedgarapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    TextView lblruta, lblorigen, lbldestino, lblcompañia, lblrecorrido;
    EditText txtruta, txtorigen, txtdestino, txtcompañia, txtrecorrido;
    Button btningresar, btnconsultar, btnmodificar, btneliminar, btngeneral;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lblruta = (TextView) findViewById(R.id.lblRuta);
        lblorigen = (TextView) findViewById(R.id.lblorigen);
        lbldestino = (TextView) findViewById(R.id.lbldestino);
        lblcompañia = (TextView) findViewById(R.id.lblcompañia);
        lblrecorrido = (TextView) findViewById(R.id.lblrecorrido);
        txtruta = (EditText) findViewById(R.id.txtruta);
        txtorigen = (EditText) findViewById(R.id.txtorigen);
        txtdestino = (EditText) findViewById(R.id.txtdestino);
        txtcompañia = (EditText) findViewById(R.id.txtcompañia);
        txtrecorrido = (EditText) findViewById(R.id.txtrecorrido);
        btningresar = (Button) findViewById(R.id.btningresar);
        btnmodificar = (Button) findViewById(R.id.btnmodificar);
        btnconsultar = (Button) findViewById(R.id.btnconsultar);
        btneliminar = (Button) findViewById(R.id.btneliminar);
        btngeneral = (Button) findViewById(R.id.btngeneral);

        btngeneral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, lista.class);
                startActivity(intent);
            }
        });

        btningresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ruta ruta = new Ruta(txtruta.getText().toString(), txtorigen.getText().toString(),txtdestino.getText().toString(),txtcompañia.getText().toString(),txtrecorrido.getText().toString());
                ruta.save();

                //Mensaje
                Toast.makeText(MainActivity.this, "Registro guardado exitosamente", Toast.LENGTH_SHORT).show();
                //Limpiar los campos de texto
                txtruta.setText("");
                txtorigen.setText("");
                txtdestino.setText("");
                txtcompañia.setText("");
                txtrecorrido.setText("");
            }
        });

        btnconsultar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    List<Ruta> resultado = Ruta.find(Ruta.class, "Ruta=?", txtruta.getText().toString());
                    Ruta datos = resultado.get(0);
                    lblruta.setText(datos.getRuta());
                    lblorigen.setText(datos.getOrigen());
                    lbldestino.setText(datos.getDestino());
                    lblcompañia.setText(datos.getCompañia());
                    lblrecorrido.setText(datos.getRecorrido());
                }catch (Exception e){
                    Log.v("error", "Se crasheo");
                }

            }
        });


        btnmodificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ruta modificar = Ruta.findById(Ruta.class, Integer.parseInt(txtruta.getText().toString()));
                modificar.ruta = txtorigen.getText().toString();
                modificar.save();

                //Mensaje
                Toast.makeText(MainActivity.this, "Registro actualizado exitosamente", Toast.LENGTH_SHORT).show();
                //Limpiar los campos de texto
                txtruta.setText("");
                txtorigen.setText("");
                txtdestino.setText("");
                txtcompañia.setText("");
                txtrecorrido.setText("");
            }
        });

        btneliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ruta eliminarRuta = Ruta.findById(Ruta.class, Integer.parseInt(txtruta.getText().toString()));
                eliminarRuta.delete();

                //Mensaje
                Toast.makeText(MainActivity.this, "Registro eliminado exitosamente", Toast.LENGTH_SHORT).show();
                //Limpiar los campos de texto
                txtruta.setText("");
            }
        });
    }
}
